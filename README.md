# jdk1.8

#### 介绍
OpenJdk1.8源码学习

jdk目录结构
```
openjdk-8-src-b132-03_mar_2014/
├── common
├── corba
├── hotspot
├── jaxp
├── jaxws
├── jdk
|   |
|   |--   
|   
├── langtools
├── make
├── nashorn
└── test

```

![输入图片说明](https://images.gitee.com/uploads/images/2020/0330/112443_008d8199_1173722.png "222.png")


1.java.lang：语言包 

2.java.util：实用包 

3.java.awt：抽象窗口工具包 

4.javax.swing：轻量级的窗口工具包，这是目前使用最广泛的GUI程序设计包 

5.java.io：输入输出包 

6.java.net：网络函数包 

7.java.applet：编制applet用到的包（目前编制applet程序时，更多的是使用swing中的JApplet类）。 

下面分别对这几个包做下介绍： 

①.java.lang：语言包 

这是Java语言的核心包，系统自动将这个包引入到用户程序，该包中主要类有： 

1.object类：它是所有类的父类，其中定义的方法其它类都可以使用。 

2.数据类型包装类：简单的数据类型的类包装，包括Integer、Float、Boolean等。 

3.数学类Math：提供常量和数学函数，包括E和PI常数及abs()、sin()、cos()、min()、max()、random()等方法，这些常量和方法都是静态的。 

4.字符串类String和StringBuffer类。 

5.系统和运行时类：System类提供一个独立于具体计算机系统资源的编程界面；Runtime类可以直接访问运行时资源。 

6.操作类 ：Class和ClassLoader类。类Class提供了对象运行时的若干信息，ClassLoader是一个抽象类，它提供了将类名转换成文件名并在文件系统中查找并装载该文件的方法。 

7.线程类：Thread类。Java是一个多线程环境，主要有Thread（线程建立）、ThreadDeath（线程结束后的清理操作）、ThreadGroup（组织一组线程）和Runnable（建立线程的交互工具）等类。 

8.错误和异常处理类：Throwable（所有错误和异常处理的父类），Exception（处理异常，需要用户捕获处理）和Error（处理硬件错误，不要求用户捕获处理）。 

9.过程类Process：它支持系统过程，当实用类Runtime执行系统命令时，会建立处理系统过程的Process类。 

②.实用包 

实用包提供了各种实用功能的类，主要包括日期类、数据结构类和随机数类等。 

1.日期类：包括Date（获取日期和时间）、Calendar（抽象类，日历类）和GregorianCalendar（Calendar类的子类）类。 

2.数据结构类：包括链表类LinkedList、向量类Vector、栈类Stack和散列表类Hashtable等。 

3.随机数类Random：它封装了Math类中的random方法，并提供了更多的辅助功能。 

③抽象窗口工具包 

Java的java.awt提供了绘图和图像类，主要用于编写GUI程序，包括按钮、标签等常用组件以及相应的事件类。 

1.组件类：包括Button，Panel，Label，Choice等类，用于设计图形界面。 

2事件包：在java.awt.event中包括各种事件处理的接口和类 

3.颜色包：在java.awt.color中提供用于颜色的类。 

4.字体包：在java.awt.font中提供用于字体相关的接口和类。 

④输入输出包 

java.io提供了系统输入输出类和接口，只要包括输入流类InputStream和输出流OutputStream就可以实现文件的输入输出、管道的数据传输以及网络数据传输的功能 

⑤网络函数包 

java.net提供了实现网络应用程序的类，主要包括用于实现Socket通信的Socket类，此外还提供了便于处理URL的类 

⑥applet包 

java.applet是专为创建Applet程序提供的包，它包含了基本的applet类和通信类，目前基本上被JApplet类所代替。 