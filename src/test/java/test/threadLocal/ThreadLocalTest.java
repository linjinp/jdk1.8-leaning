package test.threadLocal;

public class ThreadLocalTest {
	
	public static ThreadLocal<String> threadLocal = new ThreadLocal<String>();
	
	
	public static void main(String[] args) {

    	/*************************************************************************/
    	
    	threadLocal.set("当前线程设置的值  ： "+ Thread.currentThread().getName());
    	ThreadTest threadTest1 = new ThreadTest();
    	ThreadTest threadTest2 = new ThreadTest();
    	ThreadTest threadTest3 = new ThreadTest();
    	ThreadTest threadTest4 = new ThreadTest();
    	ThreadTest threadTest5 = new ThreadTest();
    	ThreadTest threadTest6 = new ThreadTest();
    	
    	Thread t1 = new Thread(threadTest1,"线程1");
    	Thread t2 = new Thread(threadTest2,"线程2");
    	Thread t3 = new Thread(threadTest3,"线程3");
    	Thread t4 = new Thread(threadTest4,"线程4");
    	Thread t5 = new Thread(threadTest5,"线程5");
    	Thread t6 = new Thread(threadTest6,"线程6");
    	t1.start();
    	t2.start();
    	t3.start();
    	t4.start();
    	t5.start();
    	t6.start();
    	
    	
    	String string = threadLocal.get();
    	
    	System.out.println(string);
	}
	
	
}
