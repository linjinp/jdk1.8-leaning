package test.fork_join.compare.jdk8Stream;

import java.time.Duration;
import java.time.Instant;
import java.util.stream.LongStream;

public class Jdk8CurrentStream {
	
	
	//方案四：采用并行流（JDK8以后的推荐做法）
    public static void main(String[] args) {

        Instant start = Instant.now();
        long result = LongStream.rangeClosed(0, 10000000L).parallel().reduce(0, Long::sum);
        Instant end = Instant.now();
        System.out.println("耗时：" + Duration.between(start, end).toMillis() + "ms");

        System.out.println("结果为：" + result); // 打印结果500500

    }
    /**
输出：
耗时：130ms
结果为：50000005000000
	*/
	

}
