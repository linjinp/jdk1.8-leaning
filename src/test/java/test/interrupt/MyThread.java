package test.interrupt;
/**
* @ClassName: MyThread  
* @Description: TODO(这里用一句话描述这个类的作用)  
* @author YUNWEN  
* @date 2020年6月3日  

当MyThread线程检测到中断标识为true后，在第9行抛出InterruptedException异常。
这样，该线程就不能再执行其他的正常语句了(如，下面的输出代码不会执行)。
这里表明：interrupt()方法有两个作用，
一个是将线程的中断状态置位(中断状态由false变成true)；
另一个则是：让被中断的线程抛出InterruptedException异常。

 */

public class MyThread extends Thread {
    @Override
    public void run() {
        super.run();
        try{
            for (int i = 0; i < 500000; i++) {
                if (this.interrupted()) {
                    System.out.println("should be stopped and exit");
                    throw new InterruptedException();
                }
                System.out.println("i=" + (i + 1));
            }
            System.out.println("this line cannot be executed. cause thread throws exception");//这行语句不会被执行!!!
        }catch(InterruptedException e){
            System.out.println("catch interrupted exception");
            e.printStackTrace();
        }
    }
}
