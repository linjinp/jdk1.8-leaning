package test.interrupt;
/**
 * 
* @ClassName: MyThreadBreak  
* @Description: TODO(这里用一句话描述这个类的作用)  
* @author YUNWEN  
* @date 2020年6月3日  

MyThreadBreak检测到了中断请求，对该中断的响应是：退出执行（或者说是结束执行）。
但是，上面第5至8行for循环，是执行break语句跳出for循环。但是，线程并没有结束，它只是跳出了for循环而已，它还会继续执下面的的代码....
因此，我们的问题是，当收到了中断请求后，如何结束该线程呢？
一种可行的方法是使用 return 语句 而不是 break语句
当然，一种更优雅的方式则是：抛出InterruptedException异常。

这是很重要的。这样，对于那些阻塞方法(比如 wait() 和 sleep())而言，当另一个线程调用interrupt()中断该线程时，
该线程会从阻塞状态退出并且抛出中断异常。这样，我们就可以捕捉到中断异常，并根据实际情况对该线程从阻塞方法中异常退出而进行一些处理。
比如说：线程A获得了锁进入了同步代码块中，但由于条件不足调用 wait() 方法阻塞了。这个时候，线程B执行 threadA.interrupt()请求中断线程A，
此时线程A就会抛出InterruptedException，我们就可以在catch中捕获到这个异常并进行相应处理(比如进一步往上抛出)
 */
public class MyThreadBreak extends Thread {
    @Override
    public void run() {
        super.run();
        for (int i = 0; i < 500000; i++) {
            if (this.interrupted()) {
                System.out.println("should be stopped and exit");
                break;
            }
            System.out.println("i=" + (i + 1));
        }
        System.out.println("this line is also executed. thread does not stopped");//尽管线程被中断,但并没有结束运行。这行代码还是会被执行
    }
}




/**


 
 
 */















